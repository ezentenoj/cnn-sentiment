[
  [
    1,
    "INTRODUCCI\u00d3N \n",
    []
  ],
  [
    2,
    "\n",
    []
  ],
  [
    3,
    "El volumen total de agua en el mundo es de 1 385 984 510 000 hm3,1.\n",
    []
  ],
  [
    4,
    "De este total, 97 % se encuentra en los oc\u00e9anos, mares y bah\u00edas; 1.7 % en las capas de hielo y glaciares; 1.6 % es agua subterr\u00e1nea y s\u00f3lo el 0.013 % (189 990 000 hm3) corresponde al agua dulce superficial l\u00edquida contenida en r\u00edos, pantanos y lagos (Gleick, 1996).\n",
    []
  ],
  [
    5,
    "Los 189 990 000 hm3 llenar\u00edan aproximadamente 189 300 000 dep\u00f3sitos de agua del tama\u00f1o del Estadio Azteca de la Ciudad de M\u00e9xico.\n",
    []
  ],
  [
    6,
    "Distribuida en el mundo por el ciclo hidrol\u00f3gico, a su vez impulsado por el flujo energ\u00e9tico, el agua interviene en la mayor\u00eda de los otros ciclos biogeoqu\u00edmicos y determina gran parte de las condiciones climatol\u00f3gicas en la troposfera2.\n",
    []
  ],
  [
    7,
    "Aunque la mayor parte del agua en el ciclo hidrol\u00f3gico terrestre es salina, es el agua dulce, con tan s\u00f3lo el 3 % del total planetario, la que ha sido un catalizador en el desarrollo de nuestras civilizaciones.\n",
    []
  ],
  [
    8,
    "Los seres humanos necesitamos del agua de forma tal que las diferencias en su disponibilidad significan grandes contrastes en el bienestar y el desarrollo.\n",
    []
  ],
  [
    9,
    "La importancia que tiene el agua, tanto para el hombre como para los procesos funcionales de los ecosistemas, est\u00e1 reflejada en el cap\u00edtulo 18 de la Agenda 21 adoptado en la Cumbre de la Tierra de R\u00edo de Janeiro en 1992, donde se indica que: \n",
    [
      {
        "text": "Cumbre",
        "type": "Nombre_cientifico",
        "offset": [
          174,
          180
        ]
      }
    ]
  ],
  [
    10,
    "...El agua se necesita en todos los aspectos de la vida.\n",
    []
  ],
  [
    11,
    "El objetivo es garantizar que se mantenga un abastecimiento adecuado de agua de buena calidad para las poblaciones, al tiempo que se preservan las funciones hidrol\u00f3gicas, bio- l\u00f3gicas y qu\u00edmicas de los ecosistemas, adaptando las actividades humanas dentro de los l\u00edmites de capacidad de la naturaleza3.\n",
    []
  ],
  [
    12,
    "Desafortunadamente, la presi\u00f3n sobre el sistema hidrol\u00f3gico tiende a aumentar junto con el crecimiento de la poblaci\u00f3n, el desarrollo econ\u00f3mico y los cambios potenciales del clima.\n",
    []
  ],
  [
    13,
    "Entre los principales retos que hay que enfrentar a nivel mundial se encuentran: la progresiva escasez relativa del agua por el incremento en la demanda y por la contaminaci\u00f3n, el lento avance hacia el abastecimiento de agua limpia y saneamiento y los da\u00f1os causados por eventos hidrometeorol\u00f3gicos extremos como sequ\u00edas, huracanes e inundaciones.\n",
    []
  ],
  [
    14,
    "La situaci\u00f3n es parad\u00f3jica ya que algunas de las acciones que se han desarrollado para superar estos y otros obst\u00e1culos, han creado nuevos problemas.\n",
    []
  ],
  [
    15,
    "Evidentemente se requiere un enfoque integral para enfrentar la complejidad de la problem\u00e1tica actual (UNESCO-OMS, 2003).\n",
    []
  ],
  [
    16,
    "En este cap\u00edtulo se presentan los principales problemas hidrol\u00f3gicos del estado y se analiza la relaci\u00f3n de esta problem\u00e1tica con otros componentes del medio f\u00edsico, bi\u00f3tico y social, desde una perspectiva ecol\u00f3gica integradora.\n",
    []
  ],
  [
    17,
    "Finalmente, se presentan las acciones que se llevan a cabo para solucionar estos problemas y las conclusiones del cap\u00edtulo.\n",
    []
  ],
  [
    18,
    "Veracruz es un estado rico en recursos naturales y con abundante agua.\n",
    []
  ],
  [
    19,
    "De las 110 regiones hidrol\u00f3gicas prioritarias identificadas por la Conabio, algunas de ellas se encuentran en Veracruz, como: r\u00edo La Antigua, r\u00edo Tecolutla, humedales del Papaloapan, San Vicente y San Juan, Los Tuxtlas, y la cuenca alta y media del Coatzacoalcos.\n",
    []
  ],
  [
    20,
    "Estas cinco regiones se definen como prioritarias debido a su alta biodiversidad y, con excepci\u00f3n de la Antigua, todas est\u00e1n amenazadas.\n",
    []
  ],
  [
    21,
    "En los casos de Los Tuxtlas, r\u00edo Tecolutla y la cuenca alta y media de Coatzacoalcos tambi\u00e9n son consideradas como prioritarias debido a la presencia de actividades productivas.\n",
    []
  ],
  [
    22,
    "Las dos \u00faltimas regiones tambi\u00e9n destacan por el desconocimiento cient\u00edfico que existe sobre ellas.\n",
    []
  ],
  [
    23,
    "En nuestro estado est\u00e1n representados una gran parte de los ecosistemas acu\u00e1ticos de M\u00e9xico, por lo que tambi\u00e9n se encuentra entre las entidades mexicanas con mayor diversidad de especies (Semarnat, 2002).\n",
    []
  ],
  [
    24,
    "En Veracruz se han encontrado 144 especies de peces que representan el 28 % de todas las especies de M\u00e9xico, de las cuales 10 se han reportado como end\u00e9micas y, entre ellas, cinco especies se encuentran en la categor\u00eda de \u201camenazadas\u201d, tres en \u201cpeligro de extinci\u00f3n\u201d y dos \u201csujetas a protecci\u00f3n especial\u201d (Mercado-Silva et al., 2010).\n",
    []
  ],
  [
    25,
    "Por otro lado, los r\u00edos con mayor diversidad de peces son el P\u00e1nuco con 75 especies (30 % end\u00e9micas), el Coatzacoalcos con 53 especies (13 % end\u00e9- micas) y el Papaloapan con 47 especies (21 % end\u00e9micas) (Aguilar, 2003).\n",
    []
  ],
  [
    26,
    "Hay otras regiones muy diversas como la regi\u00f3n de Los Tuxtlas en cuyos lagos, r\u00edos, arroyos y cascadas se reporta un total de 33 especies de peces (V\u00e1zquez et al., 2004).\n",
    []
  ],
  [
    27,
    "En cuanto a la cantidad de agua, aproximadamente escurren por el estado 121 000 millones de m3 al a\u00f1o.\n",
    []
  ],
  [
    28,
    "No obstante, Veracruz tiene importantes rezagos en la cobertura del servicio de agua y en algunas regiones como la parte norte del estado y la zona centro, las sequ\u00edas son cada vez m\u00e1s agudas.\n",
    []
  ],
  [
    29,
    "Por otro lado, hay que aclarar que la alta disponibilidad de agua no se traduce espont\u00e1neamente en beneficios inmediatos para la poblaci\u00f3n.\n",
    []
  ],
  [
    30,
    "Por ejemplo, la disponibilidad de agua y los indicadores de desarrollo socioecon\u00f3mico en la regi\u00f3n hidrol\u00f3gica Golfo X Centro muestran un comportamiento an\u00e1logo al nacional, esto es, que en las zonas de mayor disponibilidad de agua se concentra el menor desarrollo econ\u00f3mico (figura 1).\n",
    []
  ],
  [
    31,
    "Tambi\u00e9n hay que mencionar que en algunos casos el exceso de agua se convierte en un problema, ya que las inundaciones suelen ser frecuentes y el 8 % del territorio se encuentra expuesto a ellas.\n",
    []
  ],
  [
    32,
    "Sin duda, uno de los principales problemas en Veracruz es la calidad del agua.\n",
    []
  ],
  [
    33,
    "En las cinco regiones hidrol\u00f3gicas que se encuentran en el estado, las principales fuentes de contaminaci\u00f3n son las aguas residuales urbanas, industriales y agropecuarias (cuadro 1).\n",
    []
  ],
  [
    34,
    "El bajo saneamiento de las aguas residuales municipales se debe en parte a que se tienen 88 plantas de tratamiento en todo el estado, de las cuales \u00fanicamente el 28 % est\u00e1 en operaci\u00f3n, por lo que la cobertura de saneamiento es s\u00f3lo del 11.4 % (CSVA, 2005).\n",
    []
  ],
  [
    35,
    "Las descargas de origen municipal, junto con las del sector servicios, representan el 32 % del volumen descargado y el 24 % de la carga contaminante.\n",
    []
  ],
  [
    36,
    "En general, las aguas residuales industriales representan una fuente de contaminaci\u00f3n puntual de efectos muy severos.\n",
    []
  ],
  [
    37,
    "Entre las que m\u00e1s contribuyen en la entidad se encuentran los beneficios de caf\u00e9, beneficios azucareros, alcoholeras, papeleras, industrias petroqu\u00edmica y qu\u00edmica.\n",
    []
  ],
  [
    38,
    "Las industriales constituyen el 68 % del volumen descargado a r\u00edos y cauces de forma puntual, adem\u00e1s de que aportan el 76 % de la carga contaminante medida en t\u00e9rminos de la carga org\u00e1nica (DBO5).\n",
    []
  ],
  [
    39,
    "Las descargas de los ingenios azucareros y de las instalaciones de Pemex representan el 50 % del volumen generado por el sector industrial, el 65 % de la carga org\u00e1nica y el 89 % de la demanda qu\u00ed- mica de ox\u00edgeno (DQO) (CSVA, 2005).\n",
    []
  ],
  [
    40,
    "Por otro lado, las descargas provenientes de la agricultura son una fuente difusa de contaminaci\u00f3n del subsuelo, de las aguas fre\u00e1ticas y de las aguas superficiales.\n",
    []
  ],
  [
    41,
    "En Veracruz prevalece el sistema de riego de canales a cielo abierto, la mayor\u00eda de ellos sin revestir y tambi\u00e9n a nivel parcelario, donde el agua se deja correr en forma laminar sobre los terrenos de cultivo.\n",
    []
  ],
  [
    42,
    "Durante el proceso de riego, el agua excedente se infiltra, o el agua que corre superficialmente arrastra sustancias que se utilizan para la fertilizaci\u00f3n de la tierra (compuestos de nitr\u00f3geno y f\u00f3sforo) y el control de plagas (CSVA, 2005).\n",
    []
  ],
  [
    43,
    "Tambi\u00e9n existe contaminaci\u00f3n del agua proveniente de algunas sustancias usadas para el manejo veterinario del ganado y que son excretadas en forma de com- puestos metab\u00f3licamente activos, aunque se descarguen en proporciones aparentemente peque\u00f1as.\n",
    []
  ],
  [
    44,
    "En el cuadro 1 se presentan las principales fuentes de contaminaci\u00f3n de las diferentes descargas residuales sobre los sistemas acu\u00e1ticos para cada regi\u00f3n.\n",
    []
  ],
  [
    45,
    "\n",
    []
  ],
  [
    46,
    "LA VISI\u00d3N INTEGRADA DE LOS RECURSOS NATURALES \n",
    []
  ],
  [
    47,
    "\n",
    []
  ],
  [
    48,
    "La p\u00e9rdida de recursos naturales, la escasez y el exceso de agua y la deficiente calidad de \u00e9sta son resultado y forman parte de interacciones funcionales complejas dentro de las cuencas.\n",
    []
  ],
  [
    49,
    "El agua y los elementos que circulan a trav\u00e9s de las cuencas, ligan los diferentes componentes del sistema (vegetaci\u00f3n, suelo y agua), por lo que un cambio en la concen- traci\u00f3n de nutrimentos y flujos de energ\u00eda puede manifestarse en el desequilibrio de los procesos eco- l\u00f3gicos que ocurren en los diferentes sistemas.\n",
    []
  ],
  [
    50,
    "El estado de salud e integridad ecol\u00f3gica de los sistemas acu\u00e1ticos pueden verse afectados por el uso de suelo de su cuenca de drenaje y, en consecuencia, por el estado de conservaci\u00f3n de la vegetaci\u00f3n.\n",
    []
  ],
  [
    51,
    "Al ser la cuenca la unidad geogr\u00e1fica en la que ocurren la mayor parte de las fases del ciclo hidrol\u00f3gico de inter\u00e9s humano, es necesario entender dentro de ella los procesos que operan en los ecosistemas que la conforman.\n",
    []
  ],
  [
    52,
    "Dicho entendimiento propicia una visi\u00f3n integral de d\u00f3nde incorporar los componen- tes del medio social, f\u00edsico y natural que interact\u00faan a distintas escalas temporales y espaciales.\n",
    []
  ],
  [
    53,
    "S\u00f3lo a trav\u00e9s de la comprensi\u00f3n de estas interacciones, articu- ladas alrededor de la b\u00fasqueda del bienestar humano, es que ser\u00e1 posible propiciar el uso susten- table de los recursos naturales (figura 2).\n",
    []
  ],
  [
    54,
    "Dentro de estas interacciones, la relaci\u00f3n entre las funciones de los ecosistemas y el componente biol\u00f3gico es clave.\n",
    []
  ],
  [
    55,
    "El desarrollo te\u00f3rico dentro de la ecolog\u00eda y los estudios (tanto observacionales como experimentales) apoyan la idea que sostiene que los bienes y servicios que proporcionan los ecosistemas, as\u00ed como las propiedades de los cuales se derivan \u00e9stos dependen de manera fundamental de la biodi- versidad (Hooper et al., 2005, v\u00e9ase el estudio de caso 1) A continuaci\u00f3n se resaltan algunas de las principales interacciones, as\u00ed como su repercusi\u00f3n en el bienestar de los habitantes del estado.\n",
    []
  ],
  [
    56,
    "El color verde resalta procesos biol\u00f3gicos, el azul procesos f\u00edsicos, el rojo procesos causados por activi- dades humanas y el negro procesos de realimenta- ci\u00f3n hacia el bienestar humano en la regi\u00f3n.\n",
    []
  ],
  [
    57,
    "El grosor de las flechas indica escalas temporales (entre m\u00e1s anchas, menor tiempo en causar un cambio).\n",
    []
  ],
  [
    58,
    "Para facilitar la legibilidad de la figura fue necesario repetir los nombres de algunas variables, en estos casos el nombre est\u00e1 entre los signos < y >.\n",
    []
  ],
  [
    59,
    "Como se indica, la relaci\u00f3n del ser humano con los ecosiste- mas tiene un efecto significativo.\n",
    []
  ],
  [
    60,
    "La b\u00fasqueda de bienestar nos ha llevado a transformar el entorno a trav\u00e9s de distintos procesos como son: el cambio del uso del suelo hacia actividades productivas como la agricultura, la ganader\u00eda o actividades industriales; la modificaci\u00f3n y la contaminaci\u00f3n de cuerpos de agua y la emisi\u00f3n de contaminantes a la atm\u00f3sfera, entre otros.\n",
    []
  ],
  [
    61,
    "Los beneficios que han tra\u00eddo consigo estos cambios han sido a costa de alteraciones en la riqueza y la composici\u00f3n espec\u00edfica de distintos eco- sistemas y el funcionamiento hidrol\u00f3gico.\n",
    []
  ],
  [
    62,
    "Parad\u00f3jicamente, el bienestar humano muchas veces se ha empobrecido con estas acciones debido a la existencia de procesos de realimentaci\u00f3n que operan sobre algunas de las interacciones del sistema.\n",
    []
  ],
  [
    63,
    "\n",
    []
  ],
  [
    64,
    "Relaci\u00f3n entre el uso del suelo y los procesos hidrol\u00f3gicos \n",
    []
  ],
  [
    65,
    "\n",
    []
  ],
  [
    66,
    "En la parte alta de las cuencas, las coberturas forestales estabilizan el flujo subsuperficial.\n",
    []
  ],
  [
    67,
    "Este aspecto destaca sobre todo en \u00e1reas tropicales y subtropica- les como es el caso de Veracruz, donde las precipitaciones en \u00e9poca de lluvias son abundantes y de fuerte intensidad.\n",
    []
  ],
  [
    68,
    "La estructura de los suelos fores- tales, en combinaci\u00f3n con la ausencia de un uso intensivo, condiciona altas capacidades de infiltra- ci\u00f3n.\n",
    []
  ],
  [
    69,
    "En este sentido, aun durante precipitaciones de alta intensidad, gran parte del agua infiltra al suelo y la escorrent\u00eda superficial tiende a ser relativa- mente peque\u00f1a (figura 2).\n",
    []
  ],
  [
    70,
    "Por lo tanto, la deforesta- ci\u00f3n o el reemplazo de bosques y selvas por otros usos de suelo, puede modificar significativamente las tasas de intercepci\u00f3n, transpiraci\u00f3n e infiltraci\u00f3n de agua (Viles, 1990 y Le Maitre et al., 1999, cita- dos en Eviner y Chapin III, 2003).\n",
    [
      {
        "text": "Viles",
        "type": "Nombre_cientifico",
        "offset": [
          193,
          198
        ]
      }
    ]
  ],
  [
    71,
    "Lo anterior, a su vez, afecta el r\u00e9gimen de los flujos de los arroyos a lo largo del a\u00f1o y por lo tanto los patrones de inunda- ci\u00f3n y las crecidas de r\u00edos.\n",
    []
  ],
  [
    72,
    "En Veracruz, las tasas de p\u00e9rdida de bosques y selvas son alarmantes (2.7 % anual, equivalente a 5 000 ha), que lo ubican en el segundo lugar en deforestaci\u00f3n en el pa\u00eds (Challenger, com. pers.).\n",
    []
  ],
  [
    73,
    "Desde el punto de vista hidrol\u00f3gico, la conversi\u00f3n de bosques a pastizales o cultivos conduce a cambios en los patrones de flujos estacionales de los r\u00edos y aumento en los picos de descarga durante eventos de precipitaci\u00f3n (Aylward, 2004), propiciando muy probablemente inundaciones en las partes bajas.\n",
    []
  ],
  [
    74,
    "Las coberturas vegetales tambi\u00e9n brindan una adecuada protecci\u00f3n al suelo y favorecen una alta calidad del agua (regulando sedimentos, turbidez, temperatura y ox\u00edgeno disuelto).\n",
    []
  ],
  [
    75,
    "Por ejemplo, en la parte alta de la cuenca del r\u00edo La Antigua se encon- tr\u00f3 que el tipo de vegetaci\u00f3n tiene un efecto notorio en la calidad del agua.\n",
    []
  ],
  [
    76,
    "Los r\u00edos de bosque presentan un estado oligotr\u00f3fico4 y en el extremo del gra- diente, los r\u00edos del cafetal un estado eutr\u00f3fico.\n",
    []
  ],
  [
    77,
    "Los cafetales tienen una alta concentraci\u00f3n de s\u00f3lidos suspendidos totales (SST), lo que sugiere un gran arrastre de sedimentos al r\u00edo en \u00e9poca de lluvias por el alto porcentaje de suelo desnudo (Mart\u00ednez et al., 2009; V\u00e1zquez et al., en rev).\n",
    []
  ],
  [
    78,
    "En los pastizales los SST son bajos (poco suelo desnudo), pero presentan alta concentraci\u00f3n de s\u00edlice atribuido a la descom- posici\u00f3n de las paredes celulares de las gram\u00edneas (V\u00e1zquez et al., en rev).\n",
    []
  ],
  [
    79,
    "Las caracter\u00edsticas fisicoqu\u00ed- micas del agua influyen significativamente en la determinaci\u00f3n de la composici\u00f3n de especies, tanto acu\u00e1ticas como terrestres, en la parte baja de la cuenca, present\u00e1ndose as\u00ed una relaci\u00f3n compleja entre los distintos elementos que interact\u00faan dentro de \u00e9sta (figura 2).\n",
    []
  ],
  [
    80,
    "En el caso de las comunidades acu\u00e1ticas, y dado que las especies que las conforman han evolucio- nado como respuesta a los reg\u00edmenes naturales de flujo, es posible que los cambios provocados por el hombre puedan alterar su composici\u00f3n en las zonas bajas.\n",
    []
  ],
  [
    81,
    "Dentro de los factores que mayor efecto pue- den tener, en este sentido, est\u00e1n la construcci\u00f3n de presas, el azolve de r\u00edos y lagos provocado por la ero- si\u00f3n y el uso de agua dentro del \u00e1mbito agr\u00edcola, industrial y urbano (Mercado-Silva et al., 2010).\n",
    []
  ],
  [
    82,
    "Adem\u00e1s de la p\u00e9rdida potencial de especies en esta zona, es posible que la invasi\u00f3n y el \u00e9xito de especies introducidas y ex\u00f3ticas en r\u00edos tambi\u00e9n se faciliten con los cambios hidrol\u00f3gicos (figura 2).\n",
    []
  ],
  [
    83,
    "A pesar de que la investigaci\u00f3n sobre estos temas es necesaria, hasta donde se sabe, existe poca informaci\u00f3n al res- pecto sobre el estado.\n",
    []
  ],
  [
    84,
    "\n",
    []
  ],
  [
    85,
    "Riesgos naturales \n",
    []
  ],
  [
    86,
    "\n",
    []
  ],
  [
    87,
    "Dada la naturaleza costera de Veracruz, hay que considerar especialmente los procesos hidrol\u00f3gicos que ocurren en el contacto con el Golfo de M\u00e9xico.\n",
    []
  ],
  [
    88,
    "Estos procesos dan origen a un extenso y complejo mosaico de humedales de muy variados tipos y relevancia para la biodiversidad de la regi\u00f3n y para la vida humana.\n",
    []
  ],
  [
    89,
    "La composici\u00f3n de especies de estos humedales favorece la provisi\u00f3n de h\u00e1bitat para otros organismos, el reciclaje de nutrimentos y la protecci\u00f3n contra el oleaje, los nortes, las tormentas tropicales y los huracanes (Danielsen et al., 2005; UNEP-WCMC, 2006; P\u00e9rez-Maqueo et al., 2007).\n",
    []
  ],
  [
    90,
    "La funci\u00f3n de los ecosistemas naturales como barreras que protegen hasta cierto punto zonas habitadas, es trascendente bajo el escenario actual de cambio cli- m\u00e1tico.\n",
    []
  ],
  [
    91,
    "En el caso de Veracruz, el aumento en la frecuencia de huracanes de categor\u00eda mayor, debido al incremento de las temperaturas del agua (Webs- ter et al., 2005), es relevante (figura 3).\n",
    []
  ],
  [
    92,
    "De hecho, su influencia y las inundaciones asociadas a ellos, comienzan a ser un problema preocupante en el estado.\n",
    []
  ],
  [
    93,
    "El n\u00famero de ciclones tropicales (30) que se present\u00f3 en el Oc\u00e9ano Atl\u00e1ntico durante el 2005 fue superior al detectado en cualquier otro a\u00f1o del registro hist\u00f3rico, que existe para al periodo 1855- 2005 (Acevedo y Luna, 2006).\n",
    []
  ],
  [
    94,
    "Durante ese a\u00f1o, las tormentas Bret, Jos\u00e9 y el hurac\u00e1n Stan, provocaron lluvias intensas, deslaves, escurrimientos y desbordamientos de cuerpos de agua en todo el estado.\n",
    []
  ],
  [
    95,
    "Por ejemplo, la depresi\u00f3n Bret con una velocidad de vientos de 56 km/hr, produjo fuertes inundaciones por lluvia ocasionando la muerte de una persona y da\u00f1os materiales.\n",
    []
  ],
  [
    96,
    "La tormenta tropical Jos\u00e9 alcanz\u00f3 los 65 km/hr provocando inundaciones y deslaves que causaron la muerte de una persona y obligaron a que miles fueran evacuadas de sus casas.\n",
    []
  ],
  [
    97,
    "Final- mente, el hurac\u00e1n Stan de categor\u00eda 5 con velocida- des superiores a los 130 km/hr ocasion\u00f3 la muerte de 80 personas e inundaciones desastrosas en los estados de Veracruz, Oaxaca y Chiapas.\n",
    []
  ],
  [
    98,
    "Particular- mente, en la parte baja de la Cuenca de La Antigua, el impacto de Stan fue muy severo.\n",
    []
  ],
  [
    99,
    "\n",
    []
  ],
  [
    100,
    "Clima \n",
    []
  ],
  [
    101,
    "\n",
    []
  ],
  [
    102,
    "En cuanto al clima, hoy en d\u00eda se reconoce que la deforestaci\u00f3n tropical en grandes extensiones puede cambiar el clima regional significativamente (Kanae et al., 2001; Pielke et al., 2002).\n",
    []
  ],
  [
    103,
    "Se ha comprobado que los bosques tropicales inciden en las condicio- nes clim\u00e1ticas mediante distintos mecanismos, como son: el mantenimiento de una humedad ele- vada del suelo y del aire cerca de su superficie, la reducci\u00f3n de la penetraci\u00f3n de la luz al suelo y la velocidad del viento en la superficie (Pielke et al., 2002).\n",
    []
  ],
  [
    104,
    "Modelos generales de circulaci\u00f3n (MGC) sugieren que la deforestaci\u00f3n tropical puede reducir la precipitaci\u00f3n y aumentar la longitud de la \u00e9poca de secas (Shukla et al., 1990; Lawton et al., 2001; Pielke et al., 2002).\n",
    []
  ],
  [
    105,
    "Los cambios en el clima pueden propagarse incluso a nivel global.\n",
    []
  ],
  [
    106,
    "Simulaciones obtenidas a partir de MGC sugieren que cambios en el paisaje regional pueden alterar flujos de la super- ficie de la tierra en cualquier parte del mundo a tra- v\u00e9s de realimentaciones no lineales dentro de la circulaci\u00f3n atmosf\u00e9rica global (Chase et al., 2000).\n",
    []
  ],
  [
    107,
    "\n",
    []
  ],
  [
    108,
    "Cambio Clim\u00e1tico Global \n",
    []
  ],
  [
    109,
    "\n",
    []
  ],
  [
    110,
    "Hoy en d\u00eda se acepta que el Cambio Clim\u00e1tico Glo- bal est\u00e1 ocurriendo por factores antr\u00f3picos.\n",
    []
  ],
  [
    111,
    "Los modelos actuales predicen, que ante distintos esce- narios de desarrollo econ\u00f3mico, es posible que para el a\u00f1o 2020 los ecosistemas de Veracruz est\u00e9n expuestos a una variaci\u00f3n de la precipitaci\u00f3n anual de \u00b15 % y a un aumento en la temperatura anual entre 0.8 y 1.4 \u00b0C.\n",
    []
  ],
  [
    112,
    "Para el a\u00f1o 2080 la precipitaci\u00f3n podr\u00eda variar entre +10 y -20 % y la temperatura media anual incrementarse 3 \u00b0C.5 Adicionalmente, hay que considerar que el otro efecto del calentamiento global es la elevaci\u00f3n del nivel medio del mar.\n",
    []
  ],
  [
    113,
    "Por ejemplo, el sistema lagu- nar de Alvarado y del r\u00edo Papaloapan cubren un \u00e1rea de 1 183 km2 con una zona de inundaci\u00f3n de alrededor de 1 448 km2.\n",
    []
  ],
  [
    114,
    "El c\u00e1lculo del \u00e1rea de inundaci\u00f3n entre el nivel de los 0 a 1 m de altura sobre el nivel del mar indica que se encuentra un 84 % ocupada por la zona intermareal.\n",
    []
  ],
  [
    115,
    "Este nivel de inundaci\u00f3n abarca hasta 47.5 km tierra adentro sobre las tierras bajas.\n",
    []
  ],
  [
    116,
    "Para el intervalo de 1 a 2 m de elevaci\u00f3n, el \u00e1rea afectada podr\u00eda ser de 168 km2 lo cual corresponde al 16 % de la superficie total del sistema lagunar de Alvarado (Ortiz P\u00e9rez y M\u00e9ndez Linares, 1999).\n",
    []
  ],
  [
    117,
    "Debido al aumento en el nivel del mar, es muy probable que desaparezcan comunidades ligadas a \u00e1reas inundables, entre las que se encuentran los manglares ubicados en casi toda la costa,6 as\u00ed como el popal en llanuras aluvia- les al sur del estado.\n",
    []
  ],
  [
    118,
    "Hay que anotar que la mayor\u00eda de los estudios de cambio clim\u00e1tico se han enfocado a las fluctuaciones en las condiciones promedio, pero este fen\u00f3meno traer\u00e1 sobre todo cambios en la variabilidad climatol\u00f3gica, as\u00ed como en los eventos extremos, los que repercutir\u00e1n, sobre todo en el sur de M\u00e9xico, en las condiciones de vida de las personas y la permanencia o estable- cimiento de especies.\n",
    []
  ],
  [
    119,
    "\n",
    []
  ],
  [
    120,
    "RESPUESTA ESTATAL A LOS PROBLEMAS HIDROL\u00d3GICOS \n",
    []
  ],
  [
    121,
    "\n",
    []
  ],
  [
    122,
    "Como se ha se\u00f1alado, a pesar de la alta oferta de agua y la considerable biodiversidad, el estado de Veracruz no est\u00e1 exento de problemas ambientales.\n",
    []
  ],
  [
    123,
    "En respuesta, se han instrumentado pol\u00edticas dirigidas a satisfacer las necesidades h\u00eddricas de la pobla- ci\u00f3n, orientadas tambi\u00e9n a la protecci\u00f3n de otros recursos naturales.\n",
    []
  ],
  [
    124,
    "El estado de Veracruz se ha des- tacado por explorar instrumentos normativos e ins- titucionales que propicien la adecuaci\u00f3n del marco jur\u00eddico nacional y fomenten la eficacia de los meca- nismos jur\u00eddicos existentes (CSVA, 2005).\n",
    []
  ],
  [
    125,
    "A partir de la Ley N\u00famero 21 de Aguas del estado de Vera- cruz se cre\u00f3 el Sistema Veracruzano del Agua (SVA), integrado por el Ejecutivo del estado, los ayunta- mientos y los sectores social y privado.\n",
    [
      {
        "text": "Vera- cruz",
        "type": "Nombre_cientifico",
        "offset": [
          52,
          62
        ]
      }
    ]
  ],
  [
    126,
    "El SVA se encarga del tratamiento adecuado de las aguas de jurisdicci\u00f3n estatal, as\u00ed como de la prestaci\u00f3n de los servicios p\u00fablicos de agua potable, drenaje y sanea- miento.\n",
    []
  ],
  [
    127,
    "Para garantizar el cumplimiento de los objetivos establecidos en el SVA, existe una autori- dad normativa y otra operativa representadas por el Consejo del Sistema Veracruzano del Agua y por la Comisi\u00f3n del Agua del estado de Veracruz, respecti- vamente (CSVA, 2005).\n",
    []
  ],
  [
    128,
    "\n",
    []
  ],
  [
    129,
    "Programa Hidr\u00e1ulico Estatal (PHE) \n",
    []
  ],
  [
    130,
    "\n",
    []
  ],
  [
    131,
    "El Consejo del Sistema Veracruzano del Agua pre- sent\u00f3, en el 2005, el Programa Hidr\u00e1ulico Estatal (PHE).\n",
    []
  ],
  [
    132,
    "En este programa el gobierno del estado realiz\u00f3 un diagn\u00f3stico sobre la problem\u00e1tica relacionada con el agua en las cinco regiones que conforman la entidad.\n",
    []
  ],
  [
    133,
    "Los principales problemas detectados fueron: 1) la baja cobertura de agua potable y saneamiento, 2) la conta- minaci\u00f3n del agua; 3) los da\u00f1os producidos por fen\u00f3- menos hidrometeorol\u00f3gicos extremos (como inundaciones, deslaves y sequ\u00edas); 4) la baja eficiencia del uso del agua en la agricultura y la insuficiencia en la medici\u00f3n, y 5) la poca divulgaci\u00f3n de la informaci\u00f3n.\n",
    []
  ],
  [
    134,
    "El PHE contiene adem\u00e1s una prospectiva para los pr\u00f3- ximos 20 a\u00f1os, as\u00ed como los lineamientos con los cua- les se buscan nuevos esquemas y fuentes de financiamiento que permitan construir las obras nece- sarias e influir en una mayor participaci\u00f3n de la socie- dad.\n",
    []
  ],
  [
    135,
    "Como resultado del an\u00e1lisis el PHE estableci\u00f3 las acciones que deben realizar las instituciones de los dife- rentes niveles de gobierno en cada regi\u00f3n.7\n",
    []
  ],
  [
    136,
    "Dentro de las acciones a realizar, el gobierno del estado reconoce el v\u00ednculo existente entre los pro- blemas del agua con otros componentes del ecosis- tema.\n",
    []
  ],
  [
    137,
    "Adem\u00e1s de proponer ubicar las nuevas \u00e1reas protegidas y parques ecol\u00f3gicos en funci\u00f3n de aspectos hidrol\u00f3gicos, se promueve la adopci\u00f3n de Programas de Pago por Servicios Ambientales Hidrol\u00f3gicos y se busca as\u00ed proteger las fuentes de abastecimiento de las localidades.\n",
    []
  ],
  [
    138,
    "Por otro lado, el reconocimiento de la p\u00e9rdida de ecosistemas, como una de las amenazas m\u00e1s impor- tantes en la ruta hacia la sustentabilidad, ha produ- cido acciones determinantes en la entidad.\n",
    []
  ],
  [
    139,
    "Una de ellas es la promoci\u00f3n de la protecci\u00f3n de humedales a trav\u00e9s de su registro en la Convenci\u00f3n Ramsar de humedales de relevancia internacional y que ya suman nueve (cuadro 2).\n",
    []
  ],
  [
    140,
    "\n",
    []
  ],
  [
    141,
    "Iniciativa por el Agua, los Bosques y las Cuencas (Iniciativa ABC) \n",
    []
  ],
  [
    142,
    "\n",
    []
  ],
  [
    143,
    "Otro esfuerzo que puede tener resultados favorables para el estado es la Iniciativa por el Agua, los Bosques y las Cuencas (Iniciativa ABC) la que, mediante un fideicomiso, busca establecer un instrumento jur\u00eddico-financiero enfocado a la protecci\u00f3n, con- servaci\u00f3n y aprovechamiento de los recursos natura- les.\n",
    []
  ],
  [
    144,
    "Entre sus pol\u00edticas, la Iniciativa ABC instaurar\u00e1 aquellas que conciben la regulaci\u00f3n del uso del suelo con criterios ecol\u00f3gicos y promover\u00e1 activida- des productivas favorables con el ambiente, as\u00ed como el pago por servicios ambientales.8\n",
    []
  ],
  [
    145,
    "Como parte de la propuesta, la Iniciativa ABC plantea a las microcuencas como unidades b\u00e1sicas para la protecci\u00f3n, la preservaci\u00f3n y el enriqueci- miento de los ecosistemas que aseguran la biodiver- sidad y la disponibilidad de agua.\n",
    []
  ],
  [
    146,
    "Sin duda estos mecanismos suponen la modernizaci\u00f3n de los enfo- ques de administraci\u00f3n p\u00fablica en relaci\u00f3n con los recursos hidrol\u00f3gicos de la entidad.\n",
    []
  ],
  [
    147,
    "\n",
    []
  ],
  [
    148,
    "CONCLUSIONES \n",
    []
  ],
  [
    149,
    "\n",
    []
  ],
  [
    150,
    "A pesar de la importante destrucci\u00f3n de h\u00e1bitats, Veracruz sigue consider\u00e1ndose como la entidad con la tercera posici\u00f3n por riqueza biol\u00f3gica en el pa\u00eds, sin embargo, la amenaza de extinci\u00f3n o al menos de extirpaci\u00f3n estatal de muchas especies crece en forma alarmante si se considera la p\u00e9rdida de superficie de los ecosistemas originales.\n",
    []
  ],
  [
    151,
    "Es particularmente preocupante la amenaza que enfrentan los humedales de la entidad, aunque en los \u00faltimos a\u00f1os se ha empezado a revalorarlos, lo que consta por el aumento en el registro de sitios veracruzanos en la Convenci\u00f3n de Ramsar.\n",
    []
  ],
  [
    152,
    "Los costos ambientales por el deterioro de los recursos hidrol\u00f3gicos son actualmente evidentes: altos niveles de contaminaci\u00f3n, escasez de agua (con la creciente conflictividad social asociada), creciente vulnerabilidad a peligros naturales como inundaciones, sequ\u00edas, deslaves y otros.\n",
    []
  ],
  [
    153,
    "Afortunadamente la revaloraci\u00f3n de los m\u00falti- ples beneficios que los ecosistemas naturales ofrecen a la sociedad est\u00e1 ganando terreno en Veracruz.\n",
    []
  ],
  [
    154,
    "Par- ticularmente la adopci\u00f3n de esquemas que buscan reconocer el valor de los ecosistemas mediante la \u201cinternalizaci\u00f3n\u201d en la econom\u00eda, por su papel en la generaci\u00f3n de servicios ambientales hidrol\u00f3gicos (entre otros servicios que proporcionan) es una naciente realidad.\n",
    []
  ],
  [
    155,
    "Mejorar el conocimiento de los procesos ecosist\u00e9micos es esencial para gestionar y salvaguardar los recursos hidrol\u00f3gicos y naturales del estado.\n",
    []
  ],
  [
    156,
    "Este conocimiento ser\u00e1 tambi\u00e9n importante para desarrollar modelos destinados a comprender fen\u00f3menos complejos como, por ejemplo, los relacionados con el cambio clim\u00e1tico.\n",
    []
  ],
  [
    157,
    "Los modelos que se generen ser\u00e1n herramientas preventivas para mitigar las posibles consecuencias de modificaciones ambientales de gran magnitud que pueden interferir con los procesos productivos actuales.\n",
    []
  ]
]
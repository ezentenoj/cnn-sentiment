# -*- coding: utf-8 -*-
'''
Created on Dec 9, 2015

Main script to create the positive and negative numpy to train
the cnn.

@author: Daniel
'''
import os
import json
import random
import numpy as np
from scipy.misc.pilutil import imsave
import pickle
import codecs
import copy
from nltk.tokenize import word_tokenize


def convert_text_to_list(text):
    """
    Function to convert text into tokens (list of words)

    :param text: Unicode string to be converted

    :return: :class: `list` List with the string tokenized.

    """
    words_list = word_tokenize(text, language='spanish')
    return words_list


def create_dataset_features(input_dir):
    """
    Function to create the positive.pkl and negative.pkl from a directory
    containing the scientific names annotation in JSON format.

    :param input_dir: The path where all the JSON are.
                    The JSON files are created using the textmining library.
                    Look there for more information.
    """
    # Number of words to choose to train. In this case,
    # the scientific name will be placed at position 6
    words_to_choose = 11
    # Number of words to be chosen before the scientific name.
    n_words_before = 5
    positive = list()
    negative = list()
    file_names = list()
    a = u"aábcdeéfghiíjklmnñoópqrstuúvwxyz0123456789(),."
    a_list = list()
    # This file contains general Spanish lines. Add more negative
    # samples to this file if necessary.
    # TODO : eliminar líneas con nombres científicos del arch iula
    # TODO : Agregar localidades : DONE
    # iula_file = '/Users/Daniel/Downloads/annotations/es-iula.train'
    iula_file = '/Users/amolina/dock/cnn-sentiment/corpus/es-iula.train'
    with codecs.open(iula_file, mode='rb', encoding='UTF-8') as f:
        negative_content = f.readlines()
    for n in negative_content:
        words_list = convert_text_to_list(n)
        if len(words_list) >= words_to_choose:
            negative.append(words_list[:words_to_choose])
    for c in a:
        a_list.append(c)
    for root, _, files in os.walk(input_dir):
        for name in files:
            name = os.path.join(root, name)
            if os.path.splitext(name)[-1] == '.JSON':
                file_names.append(name)
                with open(name, 'rb') as f:
                    data = json.load(f)
                lines = list()
                for i, line in enumerate(data):
                    if line[0] in lines:
                        print("duplicated line")
                    else:
                        lines.append(line[0])
                    if len(line[2]) == 0:
                        # Filling up the negative list
                        try:
                            words_list = convert_text_to_list(line[1])
                            total_words = len(words_list)
                            if total_words >= words_to_choose:
                                negative.append(words_list[:words_to_choose])
                            else:
                                center_position = total_words // 2
                                words_before = n_words_before - (total_words - center_position)
                                b = 1
                                while len(words_list) < total_words + words_before:
                                    line_before_list = convert_text_to_list(data[i - b][1])
                                    words_list.reverse()
                                    for l in reversed(line_before_list):
                                        words_list.append(l)
                                        if len(words_list) >= total_words + words_before:
                                            break
                                    words_list.reverse()
                                    b += 1
                                b = 1
                                while len(words_list) < words_to_choose:
                                    line_after_list = convert_text_to_list(data[i + b][1])
                                    for l in line_after_list:
                                        words_list.append(l)
                                        if len(words_list) >= words_to_choose:
                                            break
                                    b += 1
                                if len(words_list) != words_to_choose:
                                    pass
                                negative.append(words_list)
                        except:
                            pass
                    else:
                        # Filling up the positive list
                        for category in line[2]:
                            if 'type' in category:
                                if (category['type'] == u'Nombre_cientifico' or
                                        category['type'] == u'Nombre_comun'):
                                    try:
                                        words_list = list()
                                        (start_offset, end_offset) = category['offset']
                                        name = line[1][start_offset:end_offset]
                                        name_list = convert_text_to_list(name)
                                        for n in name_list:
                                            words_list.append(n)
                                        text_before = line[1][:start_offset]
                                        text_before_list = convert_text_to_list(text_before)
                                        if len(text_before_list) >= n_words_before:
                                            words_list.reverse()
                                            for b in reversed(text_before_list):
                                                words_list.append(b)
                                                if len(words_list) >= n_words_before + len(name_list):
                                                    break
                                            words_list.reverse()
                                        else:
                                            text_before_list.reverse()
                                            b = 1
                                            while len(text_before_list) <= n_words_before:
                                                line_before_list = convert_text_to_list(data[i - b][1])
                                                for l in reversed(line_before_list):
                                                    text_before_list.append(l)
                                                    if len(text_before_list) >= n_words_before:
                                                        break
                                                b += 1
                                            words_list.reverse()
                                            for t in text_before_list:
                                                words_list.append(t)
                                            words_list.reverse()
                                        text_after = line[1][end_offset:]
                                        text_after_list = convert_text_to_list(text_after)
                                        total_after = n_words_before - (len(name_list) - 1)
                                        if len(text_after_list) >= total_after:
                                            for t in text_after_list:
                                                words_list.append(t)
                                                if len(words_list) >= words_to_choose:
                                                    break
                                        else:
                                            b = 1
                                            while len(text_after_list) <= total_after:
                                                line_after_list = convert_text_to_list(data[i + b][1])
                                                for l in line_after_list:
                                                    text_after_list.append(l)
                                                    if len(text_after) >= total_after:
                                                        break
                                                b += 1
                                            for t in text_after_list:
                                                words_list.append(t)
                                                if len(words_list) >= words_to_choose:
                                                    break
                                        if len(words_list) != words_to_choose:
                                            print(len(words_list))
                                        if category['type'] == u'Nombre_cientifico':
                                            positive.append(words_list[:words_to_choose])
                                            if len(name_list) > 1:
                                                copy_category = copy.copy(category)
                                                nombre_cientifico_s = copy_category['text'].split(' ')
                                                first_word_len = len(nombre_cientifico_s[0])
                                                copy_category['text'] = ' '.join(nombre_cientifico_s[1:])
                                                copy_category['offset'][0] += first_word_len + 1
                                                line[2].append(copy_category)
                                        else:
                                            negative.append(words_list[:words_to_choose])
                                    except Exception as _:
                                        pass
    # creating the positive numpy
    # Its shape is (total positive samples, number of available characters,
    # number of maximum characters per word, and number of words per line)
    positive_np = np.zeros((len(positive), 46, 22, words_to_choose), dtype=np.uint8)
    for i, p in enumerate(positive):
        positive_np[i, :, :, :] = words_to_features(p)
    # The positive file to be created
    with open('/Users/amolina/dock/cnn-sentiment/words_model/positive.pkl', 'wb') as f:
        pickle.dump(positive_np, f)
    print("Positive done")
    positive_np = None
    random.shuffle(negative)
    # TODO: usar parámetro para balancear el número de negativos
    negative = negative[:len(positive)*10]
    # Creating the negative numpy
    # Its shape is the same as the positive numpy
    negative_np = np.zeros((len(negative), 46, 22, words_to_choose), dtype=np.uint8)
    for i, n in enumerate(negative):
        negative_np[i, :, :, :] = words_to_features(n)
    # The negative file to be crated
    with open('/Users/amolina/dock/cnn-sentiment/words_model/negative.pkl', 'wb') as f:
        pickle.dump(negative_np, f)
    print("Negative done")


def text_as_img(text):
    """
    Function to convert a word into image

    :param text: Unicode string of a word. This will be represented
                as an image.

    :return: :class: `numpy.ndarray` The binary representation of the
                    text.

    """
    chars = u"aábcdeéfghiíjklmnñoópqrstuúvwxyz0123456789(),."
    chars_list = list()
    for c in chars:
        chars_list.append(c)
    length = 22
    text = text.lower()
    img = np.zeros((len(chars_list), length), dtype=np.uint8)
    for i, c in enumerate(text):
        try:
            pos = chars_list.index(c)
            img[pos, i] = 255
        except Exception:
            pass
    return img


def words_to_features(words):
    """
    Function to convert words into features

    :param text: A list of words to convert to features.

    :return: :class: `numpy.ndarray` The matrix representing the words.
                    Its dimensions will be (number of available characters,
                    maximum characters per word, number of words)

    """
    # These dimensions have to change depending on the problem.
    output = np.zeros((46, 22, 11), dtype=np.uint8)
    for i, w in enumerate(words):
        output[:, :, i] = text_as_img(w)
    return output


if __name__ == '__main__':
    # The input_dir is where all the json files are stored
    input_dir = '/Users/amolina/dock/cnn-sentiment/data/scientific_names'
    create_dataset_features(input_dir)
